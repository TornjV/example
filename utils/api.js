import { ajax } from "rxjs/ajax";

import { endpoint } from '@/utils';

export const signUp = (payload) =>
  ajax({
    method: "POST",
    url: endpoint("/users/signup"),
    headers: {
      "Content-Type": "application/json",
    },
    body: payload
  });

export const logIn = (payload) =>
  ajax({
    method: "POST",
    url: endpoint("/users/login"),
    headers: {
      "Content-Type": "application/json",
    },
    body: payload,
  });