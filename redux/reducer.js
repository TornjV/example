import { omit } from "lodash";
import produce from "immer";

import accountDataReducer from "./dataReducer";
import * as types from "./constants";

const initialState = {
  isSigningUp: false,
  isLoggingIn: false,
};

const accountReducer = produce((draft, action) => {
  const rest = omit(draft, Object.keys(initialState));
  const { type } = action;

  switch (type) {
    case types.SIGN_UP:
      draft.isSigningUp = true;
      return;
    case types.SIGN_UP_SUCCESS:
      draft.data = accountDataReducer(rest.data, action);
    case types.SIGN_UP_FAILURE:
      draft.isSigningUp = false;
      return;
    case types.LOG_IN:
      draft.isLoggingIn = true;
      return;
    case types.LOG_IN_SUCCESS:
      draft.data = accountDataReducer(rest.data, action);
    case types.LOG_IN_FAILURE:
      draft.isLoggingIn = false;
      return;
    default:
      draft.data = accountDataReducer(rest.data, action);
      return;
  }
}, initialState);

export default accountReducer;
