const PREFIX = "@app/account";

export const SIGN_UP = `${PREFIX}/SIGN_UP`;
export const SIGN_UP_SUCCESS = `${PREFIX}/SIGN_UP_SUCCESS`;
export const SIGN_UP_FAILURE = `${PREFIX}/SIGN_UP_FAILURE`;

export const LOG_IN = `${PREFIX}/LOG_IN`;
export const LOG_IN_SUCCESS = `${PREFIX}/LOG_IN_SUCCESS`;
export const LOG_IN_FAILURE = `${PREFIX}/LOG_IN_FAILURE`;
