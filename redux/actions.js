import * as types from "./constants";

export const signUp = (payload) => ({ type: types.SIGN_UP, payload });
export const signUpSuccess = (payload) => ({ type: types.SIGN_UP_SUCCESS, payload });
export const signUpFailure = () => ({ type: types.SIGN_UP_FAILURE });

export const logIn = (payload) => ({ type: types.LOG_IN, payload });
export const logInSuccess = (payload) => ({ type: types.LOG_IN_SUCCESS, payload });
export const logInFailure = () => ({ type: types.LOG_IN_FAILURE });
