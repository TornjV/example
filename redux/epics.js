import { of$ } from "rxjs";
import { mergeMap, map, catchError } from "rxjs/operators";
import { ofType, combineEpics } from "redux-observable";
import { toast } from 'react-toastify';

import * as actions from "./actions";
import * as types from "./constants";

import * as api from "../utils/api";

export const signUpEpic = (action$, store, apiCall = api.signUp) =>
  action$.pipe(
    ofType(types.SIGN_UP),
    mergeMap(({ payload }) =>
      apiCall(payload).pipe(
        map(({ response }) => {
          localStorage.setItem('token', response.token);
          return actions.signUpSuccess(response);
        }),
        catchError(({ response }) => {
          toast.error(response.error);    
          return of$(actions.signUpFailure());
        })
      )
    )
  );

export const logInEpic = (action$, store, apiCall = api.logIn) =>
  action$.pipe(
    ofType(types.LOG_IN),
    mergeMap(({ payload }) =>
      apiCall(payload).pipe(
        map(({ response }) => {
          localStorage.setItem('token', response.token);
          return actions.logInSuccess(response);
        }),
        catchError(({ response }) => {
          toast.error(response.error);    
          return of$(actions.logInFailure());
        })
      )
    )
  );

export default combineEpics(signUpEpic);