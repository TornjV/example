import produce from "immer";

import * as types from "./constants";

const initialState = {
  currentUser: null,
};

const accountDataReducer = produce((draft, action) => {
  const { type, payload } = action;

  switch (type) {
    case types.LOG_IN_SUCCESS:
    case types.SIGN_UP_SUCCESS:
      draft.currentUser = payload;
      return;
    default:
      return draft;
  }
}, initialState);

export default accountDataReducer;
