import { accountTypes, accountActions } from "../../redux";

import { signUpDataFixture, logInDataFixture, userFixture } from "../fixtures";

describe("Account actions", () => {
  const expectedAction = (type, payload) => ({ type, payload });

  test("Sign Up actions", () => {
    const signUpData = signUpDataFixture;
    const user = userFixture;

    expect(accountActions.signUp(signUpData)).toEqual(
      expectedAction(accountTypes.SIGN_UP, signUpData)
    );

    expect(accountActions.signUpSuccess(user)).toEqual(
      expectedAction(accountTypes.SIGN_UP_SUCCESS, user)
    );

    expect(accountActions.signUpFailure()).toEqual(
      expectedAction(accountTypes.SIGN_UP_FAILURE)
    );
  });

  test("Log In actions", () => {
    const logInData = logInDataFixture;
    const user = userFixture;

    expect(accountActions.logIn(logInData)).toEqual(
      expectedAction(accountTypes.LOG_IN, logInData)
    );

    expect(accountActions.logInSuccess(user)).toEqual(
      expectedAction(accountTypes.LOG_IN_SUCCESS, user)
    );

    expect(accountActions.logInFailure()).toEqual(
      expectedAction(accountTypes.LOG_IN_FAILURE)
    );
  });
});
