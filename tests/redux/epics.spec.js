import { Observable } from "rxjs";
import { toArray, subscribeOn } from "rxjs/operators";

import { ActionsObservable } from "redux-observable";

import {
  signUpEpic,
  logInEpic,
} from "../../redux/epics";
import { signUpDataFixture, logInDataFixture, userFixture } from "../fixtures";
import { accountActions } from "../../redux";

describe("Account epics", () => {

  test("#signUpEpic", () => {
    const signUpData = signUpDataFixture;
    const user = userFixture;

    const action$ = ActionsObservable.of(accountActions.signUp(signUpData));
    const ajax = payload =>
      Observable.of({
        response: payload,
      });

    const expectedOutputActions = [accountActions.signUpSuccess(user)];

    signUpEpic(action$, null, ajax).pipe(
      toArray(),
      subscribeOn(actualOutputActions => {
        expect(actualOutputActions).toEqual(expectedOutputActions);
      })
    );
  });

  test("#logInEpic", () => {
    const logInData = logInDataFixture;
    const user = userFixture;

    const action$ = ActionsObservable.of(accountActions.logIn(logInData));
    const ajax = payload =>
      Observable.of({
        response: payload,
      });

    const expectedOutputActions = [accountActions.logInSuccess(user)];
    
    logInEpic(action$, null, ajax).pipe(
      toArray(),
      subscribeOn(actualOutputActions => {
        expect(actualOutputActions).toEqual(expectedOutputActions);
      })
    );
  });

});
