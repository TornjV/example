import { omit } from "lodash";

import { accountTypes, accountReducer, accountActions } from "../../redux";
import accountDataReducer from "../../redux/dataReducer";

import { signUpDataFixture, logInDataFixture, userFixture } from "../fixtures";

describe("Account reducer", () => {
  let initialState;
  let rest;

  beforeEach(() => {
    initialState = {
      isSigningUp: false,
      isLoggingIn: false,
      data: {
        currentUser: null,
      },
    };

    rest = omit(initialState, Object.keys(initialState));
  });

  test("return initial state", () => {
    expect(accountReducer(undefined, {})).toEqual(initialState);
  });

  test(`should handle ${accountTypes.SIGN_UP}`, () => {
    const signUpData = signUpDataFixture;
    const expectedVisibleState = {
      ...initialState,
      isSigningUp: true,
    };

    expect(accountReducer(initialState, accountActions.signUp(signUpData))).toEqual(
      expectedVisibleState
    );
  });

  test(`should handle ${accountTypes.SIGN_UP_SUCCESS}`, () => {
    const user = userFixture;

    const newInitialState = {
      ...initialState,
      isSigningUp: true,
    };

    const expectedState = {
      ...initialState,
      isSigningUp: false,
      data: accountDataReducer(
        rest.data,
        accountActions.signUpSuccess(user)
      ),
    };

    expect(
      accountReducer(newInitialState, accountActions.signUpSuccess(user))
    ).toEqual(expectedState);
  });

  test(`should handle ${accountTypes.SIGN_UP_FAILURE}`, () => {
    const newInitialState = {
      ...initialState,
      isSigningUp: true,
    };

    const expectedState = {
      ...initialState,
      isSigningUp: false,
    };

    expect(
      accountReducer(newInitialState, accountActions.signUpFailure())
    ).toEqual(expectedState);
  });

  test(`should handle ${accountTypes.LOG_IN}`, () => {
    const logInData = logInDataFixture;
    const expectedVisibleState = {
      ...initialState,
      isLoggingIn: true,
    };

    expect(accountReducer(initialState, accountActions.logIn(logInData))).toEqual(
      expectedVisibleState
    );
  });

  test(`should handle ${accountTypes.LOG_IN_SUCCESS}`, () => {
    const user = userFixture;

    const newInitialState = {
      ...initialState,
      isLoggingIn: true,
    };

    const expectedState = {
      ...initialState,
      isLoggingIn: false,
      data: accountDataReducer(
        rest.data,
        accountActions.logInSuccess(user)
      ),
    };

    expect(
      accountReducer(newInitialState, accountActions.logInSuccess(user))
    ).toEqual(expectedState);
  });

  test(`should handle ${accountTypes.LOG_IN_FAILURE}`, () => {
    const newInitialState = {
      ...initialState,
      isLoggingIn: true,
    };

    const expectedState = {
      ...initialState,
      isLoggingIn: false,
    };

    expect(
      accountReducer(newInitialState, accountActions.logInFailure())
    ).toEqual(expectedState);
  });

});
