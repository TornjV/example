export const logInDataFixture = { email: "mail@example.com", password: "Password.123" };

export const signUpDataFixture = { fullName: "Full Name", email: "mail@example.com", password: "Password.123" };

export const userFixture = {
  id: 0,
  fullName: 'Veljko Tornjanski',
  email: 'mail@example.com',
  projects: [
    { id: 0, name: 'Project 1', timestamp: 1551279529, thumbnail: 'https://thispersondoesnotexist.com/image', data: {} },
    { id: 1, name: 'Project 2', timestamp: 1551279529, thumbnail: 'https://thispersondoesnotexist.com/image', data: {} },
    { id: 2, name: 'Project 3', timestamp: 1551279529, thumbnail: 'https://thispersondoesnotexist.com/image', data: {} },
    { id: 3, name: 'Project 4', timestamp: 1551279529, thumbnail: 'https://thispersondoesnotexist.com/image', data: {} },
    { id: 4, name: 'Project 5', timestamp: 1551279529, thumbnail: 'https://thispersondoesnotexist.com/image', data: {} },
    { id: 5, name: 'Project 6', timestamp: 1551279529, thumbnail: 'https://thispersondoesnotexist.com/image', data: {} },
  ],
};